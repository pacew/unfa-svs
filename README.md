# Do you want to [join the collaboration project](https://codeberg.org/unfa/unfa-svs/wiki/How-to-join-the-project) ?

Please use this issue board to let everyone know when you're working on a track:

https://codeberg.org/unfa/unfa-svs/projects/1001

